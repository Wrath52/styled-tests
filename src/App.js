import { useEffect, useState } from "react";
import styled from "styled-components";
import Image from "./Image";

const StyledText = styled.div`
  margin-bottom: ${({ $count }) => `${$count}px`};
`;

const useCSS = false

function App() {
  const [count, setCount] = useState(1);

  useEffect(() => {
    setTimeout(() => {
      setCount(count + 1);
    }, 1000);
  }, [count]);

  if(useCSS) {
    return <div className="App">
      <Image $count={count} useCSS/>
      <StyledText $count={count}>Перерендеры: {count}</StyledText>
    </div>
  }

  return (
    <div className="App">
      <Image $count={count} />
      <StyledText $count={count}>Перерендеры: {count}</StyledText>
    </div>
  );
}

export default App;
