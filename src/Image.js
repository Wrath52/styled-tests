import styled from "styled-components";

import image from "./image.jpeg";
import image2 from "./image2.jpeg";

import './style.css';

const StyledImage1 = styled.div`
  width: 300px;
  height: 300px;
  background-size: cover;
  background-image: ${({ $count }) =>
    $count % 2 === 0 ? `url(${image});` : `url(${image2});`};
`;

function Image(props) {
  if (props.useCSS) {
    const className = props.$count % 2 === 0 ? "image1" : "image2";
    return <div className={className} />;
  }

  return <StyledImage1 $count={props.$count} />;
}

export default Image;
